tawk_to
=======================
## REQUIREMENTS ##

- Account on tawk.to service

What Is This?
-------------

Tawk.to widget cutomization, using this module you can select widget which will be used on every page on your site

How To Use The Torrent info data
-----------------------
These are the steps you need to take in order to use this software. Order is
important.

Note: If you're not using composer_manager, then use whatever you normally use
to manage dependencies, and just install this module.
1. Download tawk_to.
2. Install module.
3. Go to module setting page admin/config/tawk/widget, login to with tawk.to credentials
4. Select widget and widget settings