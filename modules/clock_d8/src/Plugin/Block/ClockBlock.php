<?php

namespace Drupal\clock\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Datetime\Entity\DateFormat;

/**
 * Provides a 'Clock' block.
 *
 * @Block(
 *   id = "Clock_block",
 *   admin_label = @Translation("Clock"),
 * )
 */
class ClockBlock extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();

    // Initialize the variables.
    $time_zone = $this::clockGetTimezone();

    $time_zone = $config['clock_time_zone'];
    $clock_date_type = $config['clock_date_type'];

    $date_format = $this::clockDateFormat();
    $update = $config['clock_update'];
    $build = array();
    // Local timezone.
    if ($time_zone == 3 || $update == 1) {

      $local = 0;
      if ($time_zone == 3) {
        $local = 1;
        // Use the site time zone as a fallback for non-JavaScript users.
        $time_zone = date_default_timezone_get();
      }

      // Pass the needed variables to JavaScript.
      // Create a time string, from which JavaScript can create a date. The time
      // string contains the month name, which needs to be in English.
      $time = $this::clockDateNow('custom', 'F j, Y H:i:s');
      // Get the name of the offset, e.g. 'GMT'.
      $offset_name = $this::clockDateNow('custom', 'T');
      // Get the time zone offset in seconds.
      $offset_seconds = $this::clockDateNow('custom', 'Z');
      // Get Daylight Savings Time information. '1' for yes, '0' for no.
      $daylight_savings_time = $this::clockDateNow('custom', 'I');

      $js_settings = array(
        'time'                  => $time,
        'time_zone'             => $time_zone,
        'date_format'           => $date_format,
        'update'                => $update,
        'local'                 => $local,
        'offset_name'           => $offset_name,
        'offset_seconds'        => $offset_seconds,
        'daylight_savings_time' => $daylight_savings_time,
      );

      $build['#attached']['library'][] = 'clock/clock_lib';
      $build['#attached']['drupalSettings'] = $js_settings;
    }

    // Create a date object with the correct timezone.
    // and format it with thecorrect date format.
    $clock = $this::clockDateNow($clock_date_type);

    $build['#theme'] = 'clock';
    $build['#clock_time'] = $clock;
    $build['#cache'] = array('max-age' => 0);

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    // Time zone options.
    $time_zone = (!empty($config['clock_time_zone'])) ? $config['clock_time_zone'] : 2;
    // If the time zone is not set to the integer value 1, 2 or 3 corresponding.
    // to 'Site time zone', 'User time zone' and 'Local time zone' it contains.
    // the name of a custom time zone, so the time zone type must be set to.
    // integer value 4.
    $time_zone_type = ($time_zone == 1 || $time_zone == 2 || $time_zone == 3) ? $time_zone : 4;

    $custom_time_zone = date_default_timezone_get();

    $form['time_zone_type'] = array(
      '#type' => 'radios',
      '#title' => $this->t('Time zone settings'),
      '#description' => $this->t('<em>Local time zone</em> is the time zone on the operating system of the person visiting (regardless of anonymous or authenticated).'),
      '#options' => array(
        1 => $this->t('Site time zone'),
        3 => $this->t('Local time zone'),
        4 => $this->t('Custom time zone'),
      ),
      '#default_value' => $time_zone_type,
    );

    $system_date = \Drupal::config('system.date');
    $configurable_timezones = $system_date->get('timezone.user.configurable');
    // In case of user-configurable timezones show it as an option.
    if ($configurable_timezones == 1) {
      $form['time_zone_type']['#options'][2] = $this->t('User time zone');
      $form['time_zone_type']['#description'] = $this->t('<em>User time zone</em> is the time zone the Drupal user has selected on his or her profile page. <em>Local time zone</em> is the time zone on the operating system of the person visiting (regardless of anonymous or authenticated).');
      // Make the "User time zone" option appear second.
      ksort($form['time_zone_type']['#options']);
    }

    // date_timezone_names() returns an empty first row.
    $time_zone_names = system_time_zones();
    array_shift($time_zone_names);
    $form['time_zone_custom'] = array(
      '#type' => 'select',
      '#options' => $time_zone_names,
      '#default_value' => $custom_time_zone,
      // Show the select list only if "Custom time zone" is selected.
      '#states' => array(
        'visible' => array(
          ':input[name="settings[time_zone_type]"]' => array('value' => "4"),
        ),
      ),
    );

    // Date type options.
    // Format the current time with each date type for display in the form.
    // If the user has changed the time zone since the last page load,
    // the formatted time is displayed in the wrong, old time zone.
    $date_types = array();
    $date_formats = DateFormat::loadMultiple();
    foreach ($date_formats as $format) {
      $formatted_date = $this::clockDateNow($format->id(), 'custom');
      $date_types[$format->id()] = $format->label() . ' (' . $formatted_date . ')';
    }
    $form['date_type'] = array(
      '#type' => 'radios',
      '#title' => $this->t('Date type'),
      '#description' => $this->t('You can configure these date types on the <a href="@date-and-time-admin">date and time configuration page</a>.', array('@date-and-time-admin' => \Drupal::url('entity.date_format.collection'))),
      '#weight' => 3,
      '#default_value' => (!empty($config['clock_date_type'])) ? $config['clock_date_type'] : 'long',
      '#options' => $date_types,
    );

    // Update options.
    $form['update'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Update the clock every second'),
      '#description' => $this->t('This only works with JavaScript enabled.'),
      '#weight' => 5,
      '#default_value' => (!empty($config['clock_update'])) ? $config['clock_update'] : 'long',
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    // Save our custom settings when the form is submitted.
    $time_zone = $form_state->getValue('time_zone_type') == 4 ? $form_state->getValue('time_zone_custom') : $form_state->getValue('time_zone_type');
    $this->setConfigurationValue('clock_time_zone', $time_zone);
    $this->setConfigurationValue('clock_date_type', $form_state->getValue('date_type'));
    $this->setConfigurationValue('clock_update', $form_state->getValue('update'));
  }

  /**
   * Gets the correct timezone to display.
   *
   * @param string $date_format_date
   *   Whether or not the returned time zone is directly used in
   *   date_format_date(). Defaults to FALSE.
   *
   * @return string
   *   The name of the timezone, NULL if the user's time zone is to be used or
   *   'Local' if the user's local time is to be used.
   */
  protected function clockGetTimezone($date_format_date = FALSE) {
    $config = $this->getConfiguration();
    $time_zone = $config['clock_time_zone'];
    switch ($time_zone) {
      // Site time zone.
      case 1:
        $config_data_default_timezone = \Drupal::config('timezone.default');
        $time_zone = !empty($config_data_default_timezone) ? $config_data_default_timezone : @date_default_timezone_get();
        break;

      // User time zone.
      case 2:
        $user = \Drupal::currentUser();
        $config = \Drupal::config('system.date');
        if ($user && $config->get('timezone.user.configurable') && $user->isAuthenticated() && $user->getTimezone()) {
          $time_zone = $user->getTimezone();
        }
        break;

      // Local time zone.
      case 3:
        $time_zone = @date_default_timezone_get();
        break;
    }
    // If the time zone type is 'Custom time zone', $time_zone directly contains
    // the name of the time zone.
    return $time_zone;
  }

  /**
   * Gets the correct date format.
   *
   * @return string
   *   The date format of the date type used for the clock.
   */
  protected function clockDateFormat() {
    $config = $this->getConfiguration();
    $date_type = (!empty($config['clock_date_type'])) ? $config['clock_date_type'] : 'long';

    // Each date format type has a corresponding variable. If it is not set, get
    // the list of date formats for that type and use the first one.
    $date_format_entity = DateFormat::load($date_type);
    $date_format = $date_format_entity->getPattern();

    return $date_format;
  }

  /**
   * Gets the formatted date.
   *
   * @param string $type
   *   Date type string.
   * @param string $format
   *   PHP date format.
   *
   * @return string
   *   The formatted date used for the clock.
   */
  protected function clockDateNow($type, $format = '') {
    // Get current language code.
    $language_interface = \Drupal::languageManager()->getCurrentLanguage();
    $langcode = $language_interface->getId();

    $date_formatter = \Drupal::service('date.formatter');
    $formatted_date = $date_formatter->format(time(), $type, $format, $this::clockGetTimezone(TRUE), $langcode);
    return $formatted_date;
  }

}
