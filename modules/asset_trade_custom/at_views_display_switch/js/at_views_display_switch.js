(function($) {

Drupal.behaviors.atViewsDisplaySwitch = {
  attach: function(context) {
    var $views = $('.views-display-switch', context);
    $views.once('at-views-display-switch').each(function() {
      var $view = $(this);
      var viewId = $view.attr('class').match('view-id-([^ ]*)')[1];
      var $switcher = $('.views-display-switcher', $view);

      $switcher.on('click', 'a', function(e) {
        var $link = $(this);
        var state = $link.parent('li').is('.views-display-switcher-grid') ? 'grid' : 'list';

        $view.trigger('display-switch', [state]);

        e.stopPropagation();
        return false;
      });

      $view
        .on('display-switch', function(e, state) {
          var exp = new Date();
          exp.setDate(exp.getDate() + 365);
          document.cookie = 'display-switch-' + viewId + '=' + state + '; expires=' + exp.toUTCString();

          switch (state) {
            case 'grid':
              $view
                .removeClass('views-display-switch-list')
                .addClass('views-display-switch-grid');
              break;
            case 'list':
              $view
                .removeClass('views-display-switch-grid')
                .addClass('views-display-switch-list');
              break;
          }
        })
        .on('init', function() {
          var m = document.cookie.match('display-switch-' + viewId + '=([a-z]*)')
          if (m) {
            $view.trigger('display-switch', m[1]);
          }
        })
        .trigger('init');
    });
  }
};

})(jQuery);