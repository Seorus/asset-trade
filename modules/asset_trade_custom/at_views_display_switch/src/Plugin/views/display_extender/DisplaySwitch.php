<?php

namespace Drupal\at_views_display_switch\Plugin\views\display_extender;

use Drupal\views\Annotation\ViewsDisplayExtender;
use Drupal\views\Plugin\views\display_extender\DisplayExtenderPluginBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Default display extender plugin; does nothing.
 *
 * @ingroup views_display_extender_plugins
 *
 * @ViewsDisplayExtender(
 *   id = "display_switch",
 *   title = @Translation("Display Switch"),
 *   help = @Translation("Display Switch."),
 *   no_ui = FALSE
 * )
 */
class DisplaySwitch extends DisplayExtenderPluginBase {

  public function defineOptionsAlter(&$options) {
    $options['display_switch'] = array(
      'default' => FALSE,
    );
  }

  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    switch ($form_state->get('section')) {
      case 'display_switch':
        $form['#title'] = 'Display Switch';
        $form['display_switch'] = array(
          '#type' => 'checkbox',
          '#title' => t('Enable display switch'),
          '#default_value' => $this->displayHandler->getOption('display_switch'),
        );
        break;
    }
  }

  public function submitOptionsForm(&$form, FormStateInterface $form_state) {
    switch ($form_state->get('section')) {
      case 'display_switch':
        $this->displayHandler->setOption('display_switch', $form_state->getValue('display_switch'));
        break;
    }
  }

  public function optionsSummary(&$categories, &$options) {
    $enable_display_switch = $this->displayHandler->getOption('display_switch');
    $options['display_switch'] = array(
      'category' => 'other',
      'title' => t('Display Switch'),
      'value' => $enable_display_switch ? t('Yes') : t('No'),
      'desc' => t('Add some option.'),
    );
  }

}
