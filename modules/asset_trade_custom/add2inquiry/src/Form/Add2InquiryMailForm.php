<?php

/**
 * @file
 * Contains \Drupal\add2inquiry\Form\Add2InquiryMailForm.
 */

namespace Drupal\add2inquiry\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * File Add2InquiryMailForm class.
 *
 * @ingroup add2inquiry
 */
class Add2InquiryMailForm extends FormBase {

  /**
   * The mail manager.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * Constructs a new Add2InquiryMailForm.
   *
   * @param \Drupal\Core\Mail\MailManagerInterface $mail_manager
   *   The mail manager.
   */
  public function __construct(MailManagerInterface $mail_manager) {
    $this->mailManager = $mail_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('plugin.manager.mail'));
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormID() {
    return 'add2inquiry_form';
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $current_language = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $countries = \Drupal::service('country_manager')->getList();
    if ($current_language === 'de') {
      $default_country = 'DE';
    } else {
      $default_country = 'GB';        
    }
    
    $view = \Drupal\views\Views::getView('inquiry_list');  
    $view->setDisplay('block_1');
     
    $form['inquiry_list_rendered_view'] = $view->render();
    
    $form['#title'] = t('Inquiry list');
      
    $form['contact_person'] = array(
      '#markup' => t('Contact person'),
    );
      
    $form['contact_person_title'] = array(
      '#title' => t('Title'),
      '#type' => 'select',
      '#options' => array(
        'Mr' => t('Mr'),
        'Mrs' => t('Mrs'),
      ),
    );
    $form['contact_person_last_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Last name'),
      '#required' => TRUE,
    );    
    $form['contact_person_first_name'] = array(
      '#type' => 'textfield',
      '#title' => t('First name'),
      );    
    $form['contact_person_telephone'] = array(
      '#type' => 'textfield',
      '#title' => t('Telephone'),
      '#required' => TRUE,
    );    
    $form['contact_person_mobile'] = array(
      '#type' => 'textfield',
      '#title' => t('Mobile'),
    );    
    $form['contact_person_fax'] = array(
      '#type' => 'textfield',
      '#title' => t('Fax'),
    );    
    $form['contact_person_email'] = array(
      '#type' => 'textfield',
      '#title' => t('E-mail'),
      '#required' => TRUE,
    );  
    $form['contact_person_language'] = array(
      '#title' => t('Language'),
      '#type' => 'select',
      '#options' => array(
        'en' => t('English'),
        'de' => t('Deutsch'),
      ),
      '#default_value' => $current_language,
    );    

    $form['company'] = array(
      '#markup' => t('Company'),
    );

    $form['company_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#required' => TRUE,
    );    
    $form['company_street'] = array(
      '#type' => 'textfield',
      '#title' => t('Street'),
    );    
    $form['company_postcode'] = array(
      '#type' => 'textfield',
      '#title' => t('Postcode'),
      '#required' => TRUE,
    );       
    $form['company_city'] = array(
      '#type' => 'textfield',
      '#title' => t('City'),
      '#required' => TRUE,
    );      
    $form['company_country'] = array(
      '#title' => t('Country'),
      '#type' => 'select',
      '#options' => $countries,
      '#default_value' => $default_country,
    );      
    $form['company_web'] = array(
      '#type' => 'textfield',
      '#title' => t('Web'),
    );  

    $form['additional_questions'] = array(
      '#type' => 'textarea',
      '#title' => t('Additional questions'),
    );  
    $form['urgency'] = array(
      '#type' => 'radios',
      '#title' => t('Urgency'),
      '#description' => t('Are you planning to invest in a used machine on a short term basis, or just inquiring prices?'),
      '#options' => array(0 => t('Price Inquiry'), 1 => t('Short-term investment')),
    );     
    $form['technology_consulting'] = array(
      '#type' => 'radios',
      '#title' => t('Technology Consulting'),
      '#description' => t('Would you like technological advice by one of our experts?'),
      '#options' => array(0 => t('Yes, technology consulting'), 1 => t('No, no need for technology consulting')),
    );   
        
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Request quotations'),
    );
 
    return $form;
  }
  
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_values = $form_state->getValues();
    $module = 'add2inquiry';
    $key = 'inquiry_mail';
    $language_code = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $from = \Drupal::config('system.site')->get('mail');
    $to = \Drupal::config('add2inquiry.settings')->get('emailto');
    
    $view = \Drupal\views\Views::getView('inquiry_list');  
    $view->setDisplay('block_1');
    $view->render();

    $selected_nids = array();
    $selected_nodes = array();
    $selected_node_titles = array();
    foreach($view->result as $result_row) {
      $selected_nids[] = $result_row->nid; 
    }
    $node_storage = \Drupal::entityManager()->getStorage('node'); 
    $selected_nodes = $node_storage->loadMultiple($selected_nids);   
    foreach($selected_nodes as $selected_node) {
      $selected_node_titles['selected_node_titles'][] = $selected_node->getTranslation($language_code)->getTitle() . ' ' . \Drupal\Core\Url::fromRoute('entity.node.canonical', ['node' => $selected_node->id()], ['absolute' => TRUE])->toString();
    }
   
    $params = $form_values + $selected_node_titles;
    $send_now = TRUE;
    $result = $this->mailManager->mail($module, $key, $to, $language_code, $params, $from, $send_now);
    if ($result['result'] == TRUE) {
      drupal_set_message(t('Your message has been sent.'));
    }
    else {
      drupal_set_message(t('There was a problem sending your message and it was not sent.'), 'error');
    }
  }
}
