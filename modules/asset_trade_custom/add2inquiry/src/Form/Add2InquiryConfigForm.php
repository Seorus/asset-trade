<?php

/**
 * @file
 * Contains \Drupal\add2inquiry\Form\Add2InquiryConfigForm.
 */

namespace Drupal\add2inquiry\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;


class Add2InquiryConfigForm extends ConfigFormBase {

  /*
  **
  * Returns a unique string identifying the form.
  *
  * @return string
  *   The unique string identifying the form.
  */
  public function getFormId() {
    return 'availability_textimage_overlay_config_form';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */

  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('add2inquiry.settings');

    $form['add2inquiry_emailto'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Mail Inquiry List to'),
      '#default_value' => $config->get('emailto'),
      '#required' => TRUE,
    );
    
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('add2inquiry.settings');
    
    $config->set('emailto', $form_state->getValue('add2inquiry_emailto'));
    $config->save();
  }

  /**
   * Gets the configuration names that will be editable.
   *
   * @return array
   *   An array of configuration object names that are editable if called in
   *   conjunction with the trait's config() method.
   */
  protected function getEditableConfigNames() {
    return ['add2inquiry.settings'];
  }

}