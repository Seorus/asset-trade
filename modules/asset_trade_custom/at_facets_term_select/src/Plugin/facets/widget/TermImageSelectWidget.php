<?php

namespace Drupal\at_facets_term_select\Plugin\facets\widget;

use \Drupal\at_facets_term_select\Form\TermSelectWidgetForm;
use \Drupal\facets\FacetInterface;
use \Drupal\facets\Widget\WidgetPluginBase;
use \Drupal\facets\Plugin\facets\widget\ArrayWidget;
use \Drupal\Core\Form\FormStateInterface;
use \Drupal\Component\Plugin\PluginBase;
use \Drupal\taxonomy\Entity\Vocabulary;
use \Drupal\image\Entity\ImageStyle;

/**
 * The dropdown widget.
 *
 * @FacetsWidget(
 *   id = "term_image_select",
 *   label = @Translation("Term Image Select"),
 *   description = @Translation("A configurable widget that shows a select field with image support."),
 * )
 */
class TermImageSelectWidget extends TermSelectWidget {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'image_field' => NULL,
      'image_style' => NULL,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function build(FacetInterface $facet) {
    $field_id = $facet->getFieldIdentifier();
    $extra = [
      'facet' => $facet,
      'widget' => $this,
    ];

    $build = [
      '#cache' => [
        'contexts' => [
          'languages',
          'url.path',
          'url.query_args',
        ],
        'tags' => [
          'taxonomy_term',
        ],
      ],
    ];

    $form_object = \Drupal::service('class_resolver')->getInstanceFromDefinition('Drupal\at_facets_term_select\Form\TermImageSelectWidgetForm');
    $form_object->setTermSelectWidgetId($facet->getFieldIdentifier());
    $build[$field_id] = \Drupal::formBuilder()->getForm($form_object, $extra);

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state, FacetInterface $facet) {
    $config = $this->getConfiguration();

    $form += parent::buildConfigurationForm($form, $form_state, $facet);

    $options = [];

    foreach (Vocabulary::loadMultiple() as $vid => $vocabulary) {
      $vocabularies[$vid] = $vocabulary->get('name');
      $name = $vocabulary->get('name');

      foreach (\Drupal::entityManager()->getFieldDefinitions('taxonomy_term', $vid) as $field_name => $field_definition) {
        if ($field_definition->getType() == 'image') {
          $options[$name][$vid . PluginBase::DERIVATIVE_SEPARATOR . $field_name] = $field_definition->getLabel();
        }
      }
    }

    $form['term_depth_width'] = [
      '#type' => 'value',
      '#value' => NULL,
    ];

    $form['image_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Image field'),
      '#default_value' => $config['image_field'],
      '#options' => $options,
    ];

    $form['image_style'] = [
      '#type' => 'select',
      '#title' => $this->t('Image style'),
      '#default_value' => $config['image_style'],
      '#options' => image_style_options(),
    ];

    return $form;
  }

}
