<?php

namespace Drupal\at_facets_term_select\Plugin\facets\widget;

use \Drupal\at_facets_term_select\Form\TermImageSelectWidgetForm;
use \Drupal\facets\FacetInterface;
use \Drupal\facets\Widget\WidgetPluginBase;
use \Drupal\facets\Plugin\facets\widget\ArrayWidget;
use \Drupal\Core\Form\FormStateInterface;
use \Drupal\Component\Plugin\PluginBase;
use \Drupal\taxonomy\Entity\Vocabulary;
use \Drupal\image\Entity\ImageStyle;

/**
 * The dropdown widget.
 *
 * @FacetsWidget(
 *   id = "term_select",
 *   label = @Translation("Term Select"),
 *   description = @Translation("A configurable widget that shows a select field."),
 * )
 */
class TermSelectWidget extends WidgetPluginBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'term_depth_width' => NULL,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function build(FacetInterface $facet) {
    $field_id = $facet->getFieldIdentifier();
    $extra = [
      'facet' => $facet,
      'widget' => $this,
    ];

    $build = [
      '#cache' => [
        'contexts' => [
          'languages',
          'url.path',
          'url.query_args',
        ],
        'tags' => [
          'taxonomy_term',
        ],
      ],
    ];

    $form_object = \Drupal::service('class_resolver')->getInstanceFromDefinition('Drupal\at_facets_term_select\Form\TermSelectWidgetForm');
    $form_object->setTermSelectWidgetId($facet->getFieldIdentifier());
    $build[$field_id] = \Drupal::formBuilder()->getForm($form_object, $extra);

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state, FacetInterface $facet) {
    $config = $this->getConfiguration();

    $form += parent::buildConfigurationForm($form, $form_state, $facet);

    $form['term_depth_width'] = [
      '#type' => 'number',
      '#title' => $this->t('Term depth width'),
      '#description' => $this->t('Term depth width in pixels. Leave empty for no depth.'),
      '#default_value' => $config['term_depth_width'],
    ];

    return $form;
  }

}
