<?php

namespace Drupal\at_facets_term_select\Form;

use \Drupal\Core\Form\FormBase;
use \Drupal\Core\Form\FormStateInterface;
use \Drupal\Core\Url;
use \Drupal\Component\Plugin\PluginBase;
use \Drupal\taxonomy\Entity\Term;

class TermSelectWidgetForm extends FormBase {

  protected $termSelectWidgetId;

  public function setTermSelectWidgetId($id) {
    $this->termSelectWidgetId = $id;
  }

  public function getTermSelectWidgetId() {
    return $this->termSelectWidgetId;
  }

  public function getFormId() {
    return 'term_select_widget_form__' . $this->getTermSelectWidgetId();
  }

  public function buildForm(array $form, FormStateInterface $form_state, $extra = NULL) {
    $images = [];
    $urls = [];
    $options = [];
    $options_attributes = [];
    $results_tid = [];
    $facet = $extra['facet'];
    $widget = $extra['widget'];
    $results = $facet->getResults();
    $field_id = $facet->getFieldIdentifier();
    $active = $facet->getActiveItems();
    $config = $widget->getConfiguration();
    $termStorage = \Drupal::service('entity_type.manager')->getStorage('taxonomy_term');

    foreach ($results as $id => $result) {
      $tid = $result->getRawValue();
      $results_tid[$tid] = $result;

      $urls[$tid]['url'] = $result->getUrl()->setAbsolute()->toString();

      $term = Term::load($tid);
      $vid = $term->getVocabularyId();

      if (!isset($termTrees[$vid])) {
        foreach ($termStorage->loadTree($vid) as $termTree) {
          $termTrees[$vid][$termTree->tid] = $termTree;
        }
      }
    }

    foreach ($termTrees as $vocabularyTerms) {
      foreach ($vocabularyTerms as $termTree) {
        $tid = $termTree->tid;
        $depth = $termTree->depth;
        if (isset($results_tid[$tid])) {
          $options[$tid] = '';

          if ($config['term_depth_width'] && $depth) {
            $options_attributes[$tid]['data-depth'] = $depth;
            $options[$tid] = str_repeat('—', $depth) . ' ';
          }

          $options[$tid] .=
          $options_attributes[$tid]['data-term-name'] = $results_tid[$tid]->getDisplayValue();
        }
      }
    }

    $form['#attributes']['class'][] = 'term-select-widget-form';

    $form['term_select'] = [
      '#type' => 'select',
      '#multiple'  => !$facet->getShowOnlyOneResult(),
      '#options' => $options,
      '#default_value' => $active,
      '#options_attributes' => $options_attributes,
    ];
    $form['term_select']['#attributes']['class'][] = 'term-select';
    $form['term_select']['#attributes']['class'][] = 'term-select-' . $field_id;
    $form['term_select']['#attributes']['data-placeholder'] = $facet->getName();
    $form['term_select']['#attached']['library'][] = 'at_facets_term_select/drupal.facets_term_select';

    if ($config['term_depth_width']) {
      $form['term_select']['#attributes']['class'][] = 'term-select-depth';
      $form['term_select']['#attributes']['data-term-select-depth-width'] = $config['term_depth_width'];
    }

    $form['term_select_vid'] = [
      '#type' => 'value',
      '#value' => $vid,
    ];

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#attributes' => [
        'class' => [
          'search-form__submit'
        ]
      ]
    );

    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $vid = $values['term_select_vid'];

    $request = \Drupal::request();
    $query = $request->query->all();
    $query = is_array($query) ? $query : [];
    $fquery = [];

    if (isset($query['f']) && is_array($query['f'])) {
      foreach ($query['f'] as $facet) {
        list($facet_key, $facet_value) = explode(':', $facet);
        if ($facet_key !== $vid) {
          $fquery[] = $facet;
        }
      }
    }

    foreach ($values['term_select'] as $selected) {
      $fquery[] = $vid . ':' . $selected;
    }
    $query['f'] = $fquery;

    $url = \Drupal\Core\Url::fromUri('internal:/', ['query' => $query]);
    $uri = $url->setAbsolute()->toString();
    $uri = str_replace('%3A', ':', $uri);
    header("Location: $uri");
    exit;
  }

}
