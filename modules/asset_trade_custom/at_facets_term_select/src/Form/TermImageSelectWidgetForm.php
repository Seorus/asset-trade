<?php

namespace Drupal\at_facets_term_select\Form;

use \Drupal\at_facets_term_select\Form\TermSelectWidgetForm;
use \Drupal\Core\Form\FormStateInterface;
use \Drupal\Component\Plugin\PluginBase;
use \Drupal\image\Entity\ImageStyle;
use \Drupal\taxonomy\Entity\Term;

class TermImageSelectWidgetForm extends TermSelectWidgetForm {

  public function getFormId() {
    return 'term_image_select_widget_form__' . $this->getTermSelectWidgetId();
  }


  public function buildForm(array $form, FormStateInterface $form_state, $extra = NULL) {
    $form = parent::buildForm($form, $form_state, $extra);
    $images = [];
    $facet = $extra['facet'];
    $widget = $extra['widget'];
    $field_id = $facet->getFieldIdentifier();
    $config = $widget->getConfiguration();

    foreach ($facet->getResults() as $id => $result) {
      $tid = $result->getRawValue();

      list ($vocabulary, $field) = explode(PluginBase::DERIVATIVE_SEPARATOR, $config['image_field']);
      if ($field) {
        $term = Term::load($tid);
        if ($term->$field && $term->$field->entity) {
          if (isset($config['image_style'])) {
            $path = $term->$field->entity->getFileUri();
            $images[$tid]['data-img-src'] = ImageStyle::load($config['image_style'])->buildUrl($path);
          }
          else {
            $images[$tid]['data-img-src'] = $term->$field->entity->url();
          }
        }
      }
    }

    $form['term_select']['#attributes']['class'][] = 'term-image-select';
    $form['term_select']['#attributes']['class'][] = 'term-image-select-' . $field_id;
    $form['term_select']['#attached']['library'][] = 'at_facets_term_select/drupal.facets_term_image_select';
    $form['term_select']['#options_attributes'] = $images;

    return $form;
  }
}
