<?php

/**
 * @file
 * Contains \Drupal\availability_textimage_overlay\Form\ConfigForm.
 */

namespace Drupal\availability_textimage_overlay\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;


class ConfigForm extends ConfigFormBase {

  /*
  **
  * Returns a unique string identifying the form.
  *
  * @return string
  *   The unique string identifying the form.
  */
  public function getFormId() {
    return 'availability_textimage_overlay_config_form';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */

  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('availability_textimage_overlay.settings');

    $form['availabilty_textimage_overlay_sold_settings'] = array(
      '#type' => 'details',
      '#title' => $this->t('Settings for Availability Taxonomy Term "Sold"'),
      '#open' => TRUE,
    );
    
    $form['availabilty_textimage_overlay_sold_settings']['sold_tid'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Taxonomy ID for Availability Taxonomy Term "Sold"'),
      '#default_value' => $config->get('sold.tid'),
    );    
    $form['availabilty_textimage_overlay_sold_settings']['sold_en'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('English (and default) translation'),
      '#default_value' => $config->get('sold.en'),
    );
    $form['availabilty_textimage_overlay_sold_settings']['sold_de'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('German translation'),
      '#default_value' => $config->get('sold.de'),
    );
    
    $form['availabilty_textimage_overlay_reserved_settings'] = array(
      '#type' => 'details',
      '#title' => $this->t('Settings for Availability Taxonomy Term "Reserved"'),
      '#open' => TRUE,
    );
    
    $form['availabilty_textimage_overlay_reserved_settings']['reserved_tid'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Taxonomy ID for Availability Taxonomy Term "Reserved"'),
      '#default_value' => $config->get('reserved.tid'),
    );    
    $form['availabilty_textimage_overlay_reserved_settings']['reserved_en'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('English (and default) translation'),
      '#default_value' => $config->get('reserved.en'),
    );
    $form['availabilty_textimage_overlay_reserved_settings']['reserved_de'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('German translation'),
      '#default_value' => $config->get('reserved.de'),
    );
    
    
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('availability_textimage_overlay.settings');
    
    $config->set('sold.tid', $form_state->getValue('sold_tid'));
    $config->set('sold.en', $form_state->getValue('sold_en'));
    $config->set('sold.de', $form_state->getValue('sold_de'));
    
    $config->set('reserved.tid', $form_state->getValue('reserved_tid'));
    $config->set('reserved.en', $form_state->getValue('reserved_en'));
    $config->set('reserved.de', $form_state->getValue('reserved_de'));

    $config->save();
  }

  /**
   * Gets the configuration names that will be editable.
   *
   * @return array
   *   An array of configuration object names that are editable if called in
   *   conjunction with the trait's config() method.
   */
  protected function getEditableConfigNames() {
    return ['availability_textimage_overlay.settings'];
  }

}