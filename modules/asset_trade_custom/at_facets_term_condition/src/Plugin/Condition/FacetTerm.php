<?php

namespace Drupal\at_facets_term_condition\Plugin\Condition;

use \Drupal\Component\Plugin\PluginBase;
use \Drupal\Core\Form\FormStateInterface;
use \Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use \Drupal\taxonomy\Entity\Vocabulary;
use \Drupal\taxonomy\TermStorage;
use \Drupal\taxonomy\Entity\Term;
use \Drupal\facets\Plugin\Condition\OtherFacet;

/**
 * Provides an 'facet term' condition.
 *
 * This adds a condition plugin to make sure that facets can depend on other
 * facet's or their values. The facet value is a term tree select and works on
 * both the selected term and its children.
 *
 * @Condition(
 *   id = "facet_term",
 *   label = @Translation("Facet term"),
 * )
 */
class FacetTerm extends OtherFacet implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $options = [];

    foreach (Vocabulary::loadMultiple() as $vid => $vocabulary) {
      $term = \Drupal::service('entity_type.manager')->getStorage('taxonomy_term');
      $name = $vocabulary->get('name');
      foreach ($term->loadTree($vid) as $tid => $term) {
        $options[$name]["{$vid}:{$term->tid}"] = str_repeat('- ', $term->depth) . $term->name;
      }
    }

    $form['facet_value'] = [
      '#title' => $this->t('Facet value'),
      '#description' => $this->t('Only applies when a facet is already selected.'),
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => $this->configuration['facet_value'],
      '#multiple' => TRUE,
      '#weight' => -10,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate() {
    $allowed_facet_vocabularies = [];
    $allowed_facet_terms = [];
    foreach ($this->configuration['facet_value'] as $value) {
      $value = explode(':', $value);
      $allowed_facet_vocabularies[$value[0]] = $value[0];
      $allowed_facet_terms[$value[1]] = $value[1];
    }
    $allowed_facets = $this->configuration['facets'];

    // Return as early as possible when there are no settings for allowed
    // facets.
    if (empty($allowed_facets) || empty($allowed_facet_vocabularies)) {
      return TRUE;
    }

    /** @var \Drupal\facets\Plugin\Block\FacetBlock $block_plugin */
    $block_plugin = $this->blockManager->createInstance($allowed_facets);

    // Allowed facet value is not set, so we only have to check if the block is
    // shown here by running the access method on the block plugin with the
    // currently logged in user.
    if (empty($allowed_facet_terms)) {
      return $block_plugin->access($this->currentUser);
    }

    // The block plugin id is saved in the schema: BasePluginID:FacetID. This
    // means we can explode the ID on ':' and the facet id is in the last part
    // of that result.
    $block_plugin_id  = $block_plugin->getPluginId();
    $facet_id = explode(PluginBase::DERIVATIVE_SEPARATOR, $block_plugin_id)[1];

    /** @var \Drupal\facets\FacetInterface $facet */
    $facet = $this->facetStorage->load($facet_id);
    $facet = $this->facetManager->returnProcessedFacet($facet);

    foreach ($facet->getResults() as $result) {
      $raw_value = $result->getRawValue();
      if (is_numeric($raw_value)) {
        if (in_array($raw_value, $allowed_facet_terms) && $result->isActive()) {
          return TRUE;
        }

        $vocabulary = Term::load($raw_value)->getVocabularyId();
        if (in_array($vocabulary, $allowed_facet_vocabularies)) {
          $termStorage = \Drupal::service('entity_type.manager')->getStorage('taxonomy_term');
          $children = [$raw_value];
          while ($parents = $termStorage->loadParents(array_pop($children))) {
            if (!empty(array_intersect_key($allowed_facet_terms, $parents)) && $result->isActive()) {
              return TRUE;
            }

            $children += array_keys($parents);
          }
        }
      }
    }

    return FALSE;
  }

}
