<?php

/**
 * @file
 *   Drush integration for Chosen Image.
 */

/**
 * The Chosen Image plugin URI.
 */
define('AT_CHOSEN_IMAGE_DOWNLOAD_URI', 'https://github.com/djgrant/chosen-image/archive/master.zip');

/**
 * Implements hook_drush_command().
 */
function at_chosen_image_drush_command() {
  $items = array();

  // The key in the $items array is the name of the command.
  $items['at-chosen-image-plugin'] = array(
    'callback' => 'drush_at_chosen_image_plugin',
    'description' => dt('Download and install the Chosen Image plugin.'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH, // No bootstrap.
    'arguments' => array(
      'path' => dt('Optional. A path where to install the Chosen Image plugin. If omitted Drush will use the default location.'),
    ),
    'aliases' => array('chosenimageplugin'),
  );

  return $items;
}

/**
 * Implements hook_drush_help().
 */
function drush_at_chosen_drush_help($section) {
  switch ($section) {
    case 'drush:at-chosen-image-plugin':
      return dt('Download and install the Chosen Image plugin from https://github.com/djgrant/chosen-image, default location is /libraries.');
  }
}

/**
 * Command to download the Chosen plugin.
 */
function drush_at_chosen_image_plugin() {
  $args = func_get_args();
  if (!empty($args[0])) {
    $path = $args[0];
  }
  else {
    $path = 'libraries';
  }

  // Create the path if it does not exist.
  if (!is_dir($path)) {
    drush_op('mkdir', $path);
    drush_log(dt('Directory @path was created', array('@path' => $path)), 'notice');
  }

  // Set the directory to the download location.
  $olddir = getcwd();
  chdir($path);

  // Download the zip archive.
  if ($filepath = drush_download_file(AT_CHOSEN_IMAGE_DOWNLOAD_URI)) {
    $filename = basename($filepath);
    $dirname = basename($filepath, '.zip');

    // Remove any existing Chosen plugin directory.
    if (is_dir($dirname) || is_dir('chosen-image')) {
      drush_delete_dir($dirname, TRUE);
      drush_delete_dir('chosen-image', TRUE);
      drush_log(dt('A existing Chosen Image plugin was deleted from @path', array('@path' => $path)), 'notice');
    }

    // Decompress the zip archive.
    drush_tarball_extract($filename, $dirname);

    // Change the directory name to "chosen" if needed.
    if ($dirname != 'chosen-image') {
      drush_move_dir('master/chosen-image-master', 'chosen-image', TRUE);
      drush_delete_dir('master', TRUE);
      $dirname = 'chosen-image';
    }
  }

  if (is_dir($dirname)) {
    drush_log(dt('Chosen Image plugin has been installed in @path', array('@path' => $path)), 'success');
  }
  else {
    drush_log(dt('Drush was unable to install the Chosen Image plugin to @path', array('@path' => $path)), 'error');
  }

  // Set working directory back to the previous working directory.
  chdir($olddir);
}
