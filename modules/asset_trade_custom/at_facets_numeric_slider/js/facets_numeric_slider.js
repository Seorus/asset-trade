(function($) {

Drupal.behaviors.atFacetsNumericSlider = {
  attach: function(context) {
    var $numericSliders = $('.numeric-slider', context);

    $numericSliders.once('at-facets-numeric-slider').each(function() {
      var $numericSliderFieldset = $(this);
      var $form = $numericSliderFieldset.parents('form');
      var $numericSliderFields = $numericSliderFieldset.find('input');
      var $minField = $numericSliderFields.filter('[name="numeric_slider_min"]');
      var $maxField = $numericSliderFields.filter('[name="numeric_slider_max"]');
      var $numericSlider = $numericSliderFieldset.after('<div class="numeric-slider-jquery-ui-slider"></div>').next('.numeric-slider-jquery-ui-slider');
      var $numericSliderText = $numericSlider.after('<div class="numeric-slider-jquery-ui-slider-text"></div>').next('.numeric-slider-jquery-ui-slider-text');

      var prefix = $numericSliderFieldset.data('prefix');
      var suffix = $numericSliderFieldset.data('suffix');
      var $numericSliderTextMin = $numericSliderText.append(prefix + '<span class="slider-min">' + $minField.val() + '</span>' + suffix + ' - ').children('.slider-min');
      var $numericSliderTextMax = $numericSliderText.append(prefix + '<span class="slider-max">' + $maxField.val() + '</span>' + suffix).children('.slider-max');

      var minRange = $minField.data('slider-range');
      var maxRange = $maxField.data('slider-range');

      var minDefault = $minField.val();
      var maxDefault = $maxField.val();

      var steps = $numericSliderFieldset.data('steps');

      var nonInt = !($numericSliderFieldset.data('integer'));

      if (nonInt) {
        minRange = minRange * 10;
        maxRange = maxRange * 10;

        minDefault = minDefault * 10;
        maxDefault = maxDefault * 10;

        steps = steps * 10;
      }

      $numericSliderFieldset.hide();
      $numericSlider.slider({
        range: true,
        min: minRange,
        max: maxRange,
        step: steps,
        values: [ minDefault, maxDefault ],
        slide: function(e, ui) {
          var min = ui.values[0];
          var max = ui.values[1];

          if (nonInt) {
            min = min / 10;
            max = max / 10;
          }

          $numericSliderTextMin.text(min);
          $numericSliderTextMax.text(max);
        },
        change: function(e, ui) {
          var min = ui.values[0];
          var max = ui.values[1];

          if (nonInt) {
            min = min / 10;
            max = max / 10;
          }

          $minField.val(min);
          $maxField.val(max);
          $form.addClass('changed');
        }
      });
    });
  }
};

})(jQuery);
