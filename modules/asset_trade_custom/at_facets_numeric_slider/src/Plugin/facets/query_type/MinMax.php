<?php

namespace Drupal\at_facets_numeric_slider\Plugin\facets\query_type;

use \Drupal\facets\QueryType\QueryTypePluginBase;
use \Drupal\facets\Result\Result;
use \Drupal\facets\Plugin\facets\query_type\SearchApiString;

/**
 * Provides support for min/max facet search.
 *
 * @FacetsQueryType(
 *   id = "at_facets_numeric_slider_minmax",
 *   label = @Translation("Min/Max"),
 * )
 */
class MinMax extends QueryTypePluginBase {

  /**
   * The backend's native query object.
   *
   * @var \Drupal\search_api\Query\QueryInterface
   */
  protected $query;

  /**
   * {@inheritdoc}
   */
  public function execute() {
    $query = $this->query;

    // Only alter the query when there's an actual query object to alter.
    if (!empty($query)) {
      $operator = $this->facet->getQueryOperator();
      $field_identifier = $this->facet->getFieldIdentifier();
      $exclude = $this->facet->getExclude();

      // Set the options for the actual query.
      $options = &$query->getOptions();
      $options['search_api_facets'][$field_identifier] = array(
        'field' => $field_identifier,
        'limit' => 0,
        'operator' => $this->facet->getQueryOperator(),
        'min_count' => 0,
        'missing' => FALSE,
      );

      // Add the filter to the query if there are active values.
      $active_items = $this->facet->getActiveItems();

      if (count($active_items)) {
        $filter = $query->createConditionGroup($operator, ['facet:' . $field_identifier]);
        foreach ($active_items as $item) {
          $value = explode(':', $item);
          if (count($value) == 2) {
            $min = $value[0];
            $max = $value[1];
            $widget = $this->facet->getWidget();
            if ($widget['config']['date']) {
              $min = mktime(0, 0, 0, 0, 0, $min) - 1;
              $max = mktime(0, 0, 0, 0, 0, $max + 1) - 1;
            }
            $filter->addCondition($this->facet->getFieldIdentifier(), $min, '>=');
            $filter->addCondition($this->facet->getFieldIdentifier(), $max, '<=');
          }
        }
        $query->addConditionGroup($filter);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    if (!empty($this->results)) {
      $facet_results = array();
      foreach ($this->results as $key => $result) {
        $count = $result['count'];
        $result_filter = trim($result['filter'], '"');
        $result = new Result($result_filter, $result_filter, $count);
        $facet_results[] = $result;
      }
      $this->facet->setResults($facet_results);
    }

    return $this->facet;
  }

}
