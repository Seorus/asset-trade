<?php

namespace Drupal\at_facets_numeric_slider\Plugin\facets\widget;

use \Drupal\at_facets_numeric_slider\Form\NumericSliderWidgetForm;
use \Drupal\facets\FacetInterface;
use \Drupal\facets\Widget\WidgetPluginBase;
use \Drupal\facets\Plugin\facets\widget\ArrayWidget;
use \Drupal\Core\Form\FormStateInterface;
use \Drupal\Component\Plugin\PluginBase;
use \Drupal\taxonomy\Entity\Vocabulary;
use \Drupal\image\Entity\ImageStyle;

/**
 * The numeric slider widget.
 *
 * @FacetsWidget(
 *   id = "numeric_slider",
 *   label = @Translation("Numeric Slider"),
 *   description = @Translation("A configurable widget that shows a numeric slider."),
 * )
 */
class NumericSliderWidget extends WidgetPluginBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'date' => NULL,
      'steps' => NULL,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function build(FacetInterface $facet) {
    $field_id = $facet->getFieldIdentifier();
    $extra = [
      'facet' => $facet,
      'widget' => $this,
    ];

    $build = [
      '#cache' => [
        'contexts' => [
          'languages',
          'url.path',
          'url.query_args',
        ],
        'tags' => [
          'taxonomy_term',
        ],
      ],
    ];

    $form_object = \Drupal::service('class_resolver')->getInstanceFromDefinition('Drupal\at_facets_numeric_slider\Form\NumericSliderWidgetForm');
    $form_object->setFormWidgetId($facet->getFieldIdentifier());
    $build[$field_id] = \Drupal::formBuilder()->getForm($form_object, $extra);

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state, FacetInterface $facet) {
    $config = $this->getConfiguration();

    $form += parent::buildConfigurationForm($form, $form_state, $facet);

    $form['steps'] = [
      '#type' => 'number',
      '#title' => $this->t('Steps'),
      '#description' => $this->t('Number of steps to slide by'),
      '#default_value' => $config['steps'],
    ];

    $form['date'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Format as a date'),
      '#default_value' => $config['date'],
    ];

    return $form;
  }

  public function getQueryType($query_types) {
    return $query_types['minmax'];
  }

}
