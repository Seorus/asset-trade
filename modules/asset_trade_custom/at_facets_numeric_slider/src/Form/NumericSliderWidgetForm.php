<?php

namespace Drupal\at_facets_numeric_slider\Form;

use \Drupal\Core\Form\FormBase;
use \Drupal\Core\Form\FormStateInterface;
use \Drupal\Core\Url;
use \Drupal\Component\Plugin\PluginBase;
use \Drupal\taxonomy\Entity\Term;
use \Drupal\field\Entity\FieldStorageConfig;
use \Drupal\field\Entity\FieldConfig;

class NumericSliderWidgetForm extends FormBase {

  protected $formWidgetId;

  public function setFormWidgetId($id) {
    $this->formWidgetId = $id;
  }

  public function getFormWidgetId() {
    return $this->formWidgetId;
  }

  public function getFormId() {
    return 'numeric_slider_form__' . $this->getFormWidgetId();
  }

  public function buildForm(array $form, FormStateInterface $form_state, $extra = NULL) {
    $facet = $extra['facet'];
    $widget = $extra['widget'];
    $results = $facet->getResults();
    $field_id = $facet->getFieldIdentifier();
    $url_alias = $facet->getUrlAlias();
    $active = $facet->getActiveItems();
    $config = $widget->getConfiguration();

    foreach ($results as $id => $result) {
      $values[] = $result->getRawValue();
    }
    sort($values);

    $prefix =
    $suffix = '';
    $integer = TRUE;
    if ($field_info = FieldStorageConfig::loadByName('node', $field_id)) {
      $integer = $field_info->getType() == 'integer';
      $bundle = current($field_info->getBundles());
      $field_instance = FieldConfig::loadByName('node', $bundle, $field_id);
      $prefix = $field_instance->getSetting('prefix') ? $field_instance->getSetting('prefix') : '';
      $suffix = $field_instance->getSetting('suffix') ? $field_instance->getSetting('suffix') : '';
    }

    $default_min = $values[0];
    $default_max = end($values);
    $slider_min = $default_min;
    $slider_max = $default_max;

    if ($config['date']) {
      $integer = TRUE;
      $default_min = date('Y', $default_min);
      $default_max = date('Y', $default_max);
      $slider_min = date('Y', $slider_min);
      $slider_max = date('Y', $slider_max);
    }

    if ($config['steps']) {
      $default_min = floor($default_min / $config['steps']) * $config['steps'];
      $default_max = ceil($default_max / $config['steps']) * $config['steps'];

      $slider_min = floor($slider_min / $config['steps']) * $config['steps'];
      $slider_max = ceil($slider_max / $config['steps']) * $config['steps'];
    }

    foreach ($active as $active_item) {
      $value = explode(':', $active_item);
      if (count($value) == 2) {
        $min = $value[0];
        $max = $value[1];

        $default_min = ($min >= $slider_min && $min <= $slider_max) ? $min : $slider_min;
        $default_max = ($max >= $slider_min && $max <= $slider_max) ? $max : $slider_max;
      }
    }

    $form['#attributes']['class'][] = 'numeric-slider-form';

    $form['numeric_slider'] = [
      '#type' => 'fieldset',
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
      '#attributes' => [
        'data-prefix' => $prefix,
        'data-suffix' => $suffix,
        'data-integer' => (int) $integer,
        'data-steps' => $config['steps'],
        'class' => [
          'numeric-slider',
        ],
      ],
      '#attached' => [
        'library' => [
          'at_facets_numeric_slider/drupal.facets_numeric_slider',
        ],
      ],
    ];
    $form['numeric_slider']['numeric_slider_min'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Min'),
      '#default_value' => $default_min,
      '#attributes' => [
        'data-slider-range' => $slider_min,
      ],
    ];
    $form['numeric_slider']['numeric_slider_max'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Max'),
      '#default_value' => $default_max,
      '#attributes' => [
        'data-slider-range' => $slider_max,
      ],
    ];

    $form['numeric_slider_id'] = [
      '#type' => 'value',
      '#value' => $url_alias,
    ];

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#attributes' => [
        'class' => [
          'search-form__submit'
        ]
      ]
    );

    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $min = $values['numeric_slider_min'];
    $max = $values['numeric_slider_max'];
    $id = $values['numeric_slider_id'];

    $request = \Drupal::request();
    $query = $request->query->all();
    $query = is_array($query) ? $query : [];
    $fquery = [];

    if (isset($query['f']) && is_array($query['f'])) {
      foreach ($query['f'] as $facet) {
        $facet_array = explode(':', $facet);
        if ($facet_array[0] !== $id) {
          $fquery[] = $facet;
        }
      }
    }

    $fquery[] = $id . ':' . $min . ':' . $max;
    $query['f'] = $fquery;

    $url = \Drupal\Core\Url::fromUri('internal:/', ['query' => $query]);
    $uri = $url->setAbsolute()->toString();
    $uri = str_replace('%3A', ':', $uri);
    header("Location: $uri");
    exit;
  }

}
