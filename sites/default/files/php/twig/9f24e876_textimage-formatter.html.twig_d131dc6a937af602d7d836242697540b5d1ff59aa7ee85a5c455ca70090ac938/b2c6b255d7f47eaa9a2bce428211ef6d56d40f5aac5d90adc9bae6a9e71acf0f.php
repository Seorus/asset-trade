<?php

/* modules/textimage/templates/textimage-formatter.html.twig */
class __TwigTemplate_2213f5d4eb4e3830375ddba0fc9a600adb87381c1ef232743d0309e03c85dc17 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("spaceless" => 12, "if" => 13);
        $filters = array("t" => 28);
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array('spaceless', 'if'),
                array('t'),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setTemplateName($this->getTemplateName());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 12
        ob_start();
        // line 13
        if (($context["image"] ?? null)) {
            // line 14
            echo "  ";
            if (($context["anchor_url"] ?? null)) {
                // line 15
                echo "    <a ";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["anchor_attributes"] ?? null), "html", null, true));
                echo ">
  ";
            }
            // line 17
            echo "    ";
            if (($context["image_container_attributes"] ?? null)) {
                // line 18
                echo "      <div ";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["image_container_attributes"] ?? null), "html", null, true));
                echo ">
    ";
            }
            // line 20
            echo "    ";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["image"] ?? null), "html", null, true));
            echo "
    ";
            // line 21
            if (($context["image_container_attributes"] ?? null)) {
                // line 22
                echo "      </div>
    ";
            }
            // line 24
            echo "  ";
            if (($context["anchor_url"] ?? null)) {
                // line 25
                echo "    </a>
  ";
            }
        } else {
            // line 28
            echo "  ";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Image not available.")));
            echo "
";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    public function getTemplateName()
    {
        return "modules/textimage/templates/textimage-formatter.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  84 => 28,  79 => 25,  76 => 24,  72 => 22,  70 => 21,  65 => 20,  59 => 18,  56 => 17,  50 => 15,  47 => 14,  45 => 13,  43 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "modules/textimage/templates/textimage-formatter.html.twig", "/var/www/vhosts/asset-trade.net/httpdocs/modules/textimage/templates/textimage-formatter.html.twig");
    }
}
