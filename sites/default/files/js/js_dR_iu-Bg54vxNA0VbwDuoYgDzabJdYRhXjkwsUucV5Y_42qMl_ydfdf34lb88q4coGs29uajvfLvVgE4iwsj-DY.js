(function($) {

Drupal.behaviors.atFacetsTermImageSelectChosen = {
  attach: function(context, settings) {
    $.each(settings.termImageSelectImages, function(field, images) {
      var $select = $('.term-image-select-' + field, context);

      $select.once('facets-term-image-select')
        .each(function() {
          var $options = $(this).find('option');

          $options.each(function() {
            var $option = $(this);
            var tid = $option.attr('value');

            if (images[tid] && images[tid]) {
              $option.attr('data-img-src', images[tid]);
            }
          });

          $select
            .chosen('destroy')
            .chosenImage({
              'width': '100%'
            });
        });
    });
  }
};

})(jQuery);
;
