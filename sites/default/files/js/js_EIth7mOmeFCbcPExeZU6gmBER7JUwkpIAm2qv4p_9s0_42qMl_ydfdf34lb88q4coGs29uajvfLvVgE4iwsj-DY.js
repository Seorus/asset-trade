(function($) {

Drupal.behaviors.atFacetsTermSelect = {
  attach: function(context, settings) {
    var $selects = $('select.term-select', context);

    $selects.once('facets-term-select')
      .each(function() {
        var $select = $(this);
        var $form = $select.parents('form');
        var $options = $(this).find('option');
        var loaded = [];

        $options.each(function() {
          var $option = $(this);
          if ($option.attr('selected')) {
            loaded.push($option.attr('value'));
          }
        });

        $select
          .on('change', function() {
            if ($select.val() && $select.val().length !== loaded.length) {
              $form.submit();
            }
          })
          .chosen('destroy')
          .chosen({
            'width': '100%',
          });
      });
  }
};

Drupal.behaviors.atFacetsTermSelectDepth = {
  attach: function(context, settings) {
    $.each(settings.termSelectDepth, function(field, depth) {
      var $select = $('.term-select-' + field, context);
      var $results = $select.next('.chosen-container').find('.chosen-results');

      $select.once('facets-term-select-depth')
        .on('chosen:showing_dropdown', function() {
          $results.find('li').each(function() {
            var $option = $(this);
            var tid = $select.find('option:eq(' + $option.attr('data-option-array-index') + ')').attr('value');

            if (depth.terms[tid]) {
              $option
                .addClass('term-depth-child term-depth-' + depth.terms[tid])
                .css('padding-left', (depth.width * depth.terms[tid]) + 'px');
            }
          });
        });
    });
  }
};

})(jQuery);
;
(function($) {

Drupal.behaviors.atFacetsTermImageSelectChosen = {
  attach: function(context, settings) {
    $.each(settings.termImageSelectImages, function(field, images) {
      var $select = $('.term-image-select-' + field, context);

      $select.once('facets-term-image-select')
        .each(function() {
          var $options = $(this).find('option');

          $options.each(function() {
            var $option = $(this);
            var tid = $option.attr('value');

            if (images[tid] && images[tid]) {
              $option.attr('data-img-src', images[tid]);
            }
          });

          $select
            .chosen('destroy')
            .chosenImage({
              'width': '100%'
            });
        });
    });
  }
};

})(jQuery);
;
