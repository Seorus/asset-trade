(function($) {

Drupal.behaviors.atFacetsTermImageSelectChosen = {
  attach: function(context, settings) {
    var $selects = $('.term-image-select', context);
    $selects.each(function() {
      var $select = $(this);

      $select.once('facets-term-image-select')
        .chosen('destroy')
        .chosenImage({
          'width': '100%'
        });
    });
  }
};

})(jQuery);
;
