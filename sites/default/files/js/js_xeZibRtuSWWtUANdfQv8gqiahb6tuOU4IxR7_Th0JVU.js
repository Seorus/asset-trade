(function($) {

Drupal.behaviors.atFacetsTermSelect = {
  attach: function(context, settings) {
    var $selects = $('select.term-select', context);

    $selects.once('facets-term-select')
      .each(function() {
        var $select = $(this);
        var $form = $select.parents('form');
        var $options = $(this).find('option');
        var $block = $select.closest('.block');
        var $title = $block.children('h2');
        var loaded = [];
        var chosen = $select.next('.chosen-container').length;

        if ($title.length) {
          $select.attr('data-placeholder', $title.text());
        }

        $options.each(function() {
          var $option = $(this);
          if ($option.attr('selected')) {
            loaded.push($option.attr('value'));
          }
          if (chosen && $option.data('term-name')) {
            $option.text($option.data('term-name'));
          }
        });

        if (!chosen) {
          $block.addClass('facets-chosen-disabled');
        }

        $select
          .on('change', function() {
            if ($select.val() && $select.val().length !== loaded.length) {
              $form.addClass('changed');
            }
          })
          .chosen('destroy')
          .chosen({
            'width': '100%',
          });

      });
  }
};

Drupal.behaviors.atFacetsTermSelectDepth = {
  attach: function(context, settings) {
    var $selects = $('.term-select', context);
    $selects.each(function() {
      var $select = $(this);
      var $results = $select.next('.chosen-container').find('.chosen-results');
      var depthWidth = $select.data('term-select-depth-width');

      $select.once('facets-term-select-depth')
        .on('chosen:showing_dropdown', function() {
          $results.find('li').each(function() {
            var $option = $(this);
            var depth = $select.find('option:eq(' + $option.data('option-array-index') + ')').data('depth');

            if (depth) {
              $option
                .addClass('term-depth-child term-depth-' + depth)
                .css('padding-left', (depthWidth * depth) + 'px');
            }
          });
        });
    });
  }
};

})(jQuery);
;
(function($) {

Drupal.behaviors.atFacetsTermImageSelectChosen = {
  attach: function(context, settings) {
    var $selects = $('.term-image-select', context);
    $selects.each(function() {
      var $select = $(this);

      $select.once('facets-term-image-select')
        .chosen('destroy')
        .chosenImage({
          'width': '100%'
        });
    });
  }
};

})(jQuery);
;
