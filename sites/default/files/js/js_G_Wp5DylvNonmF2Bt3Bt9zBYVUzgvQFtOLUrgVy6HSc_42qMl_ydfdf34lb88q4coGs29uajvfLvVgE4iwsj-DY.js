(function($) {

Drupal.behaviors.atFacetsTermSelectChosenImage = {
  attach: function(context, settings) {
    $.each(settings.termImageSelect, function(field, data) {
      var $select = $('.term-image-select-' + field, context);
      var $input;

      $select.once('term-image-select')
        .bind('change', function() {
          var tid = $select.val();

          if (data[tid] && data[tid].url) {
            window.location.href = data[tid].url;
          }
        })
        .each(function() {
          var $options = $(this).find('option');

          $options.each(function() {
            var $option = $(this);
            var tid = $option.attr('value');

            if (data[tid]) {
              if (data[tid].image) {
                $option.attr('data-img-src', data[tid].image);
              }
              if (data[tid].active) {
                $option.attr('selected', 'selected');
              }
            }
          });

          $select
            .chosen('destroy')
            .chosenImage({
              'width': '100%'
            });

          // $input = $select.next('.chosen-container').find('.search-field input');
          // console.log($input);
          // $input
          //   .attr('placeholder', $select.attr('placeholder'));
        });
    });
  }
};

})(jQuery);
;
